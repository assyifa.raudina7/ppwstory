from django import forms

from .models import Activity, People

class PeopleForm(forms.ModelForm):
    class Meta:
        model = People
        fields = ['name']
        widgets = {
            'nama':forms.TextInput(attrs={'class': 'form-control'})
        }

class ActivityForm(forms.ModelForm):
    class Meta:
        model = Activity
        fields = ['nama']
        widgets = {
            'name':forms.TextInput(attrs={'class': 'form-control'})
        }




