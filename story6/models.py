from django.db import models

# Create your models here.


class Activity(models.Model):
    nama = models.CharField(max_length=100, blank=False, null=False)

    def _str_(self):
        return self.nama


class People(models.Model):
    name = models.CharField(max_length=100, blank=False, null=True)
    activity = models.ForeignKey(Activity, null=True, on_delete= models.CASCADE)

    def _str_(self):
        return self.name
    

    