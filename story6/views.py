from django.http import HttpResponseRedirect
from django.shortcuts import redirect, render
from .models import *
from .forms import PeopleForm, ActivityForm

# Create your views here.
def story6(request):
    activities = Activity.objects.all()
    actform = ActivityForm(request.POST or None)
    jumlahact = activities.count()
    peserta = People.objects.all()

    if actform.is_valid():
        actform.save()
        actform = ActivityForm()

    
    context = {
            'peserta': peserta,
            'activities': activities,
            'actform': actform,
            'jumlahact': jumlahact
    }

    return render(request, 'storysix.html', context)

def peepform(request, id):
    peepsform = PeopleForm(request.POST or None)

    if peepsform.is_valid():
        act = Activity.objects.get(id=id)
        People.objects.create(name=request.POST['name'], activity=act)
        peepsform = PeopleForm()

    context = {
                'peepsform': peepsform
    }

    return render(request, 'pesertaform.html', context)









    



