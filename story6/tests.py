from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import story6, peepform
from .models import Activity, People
from .forms import ActivityForm, PeopleForm


# Create your tests here.
class TestStorySix(TestCase):
    def test_url_story6_exists(self):
        #mencek url story 6 
        response = Client().get('/storysix/')
        self.assertEqual(response.status_code, 200)
    
    def test_story6_def_storysix(self):
        found = resolve('/storysix/')
        self.assertEqual(found.func, story6)

    def test_can_create_model_activity_and_people(self):
        #create new activity
        new_activity = Activity.objects.create(nama='Painting')
        #retrieve activity
        counting_all_available_activity = Activity.objects.all().count()
        #add participant to activity
        new_participant = People.objects.create(name='Syifa', activity=new_activity)
        counting_all_available_participants = People.objects.all().count()

        check = People.objects.filter(activity=new_activity).count()

        self.assertEqual(counting_all_available_activity, 1)
        self.assertEqual(counting_all_available_participants, 1)
        self.assertEqual(check, 1)
        self.assertEqual(new_activity._str_(), 'Painting')
        self.assertEqual(new_participant._str_(), 'Syifa')

    def test_form_validation_for_blank(self):
        actform = ActivityForm(data={'nama': ''})
        peepform = PeopleForm(data={'name': ''})
        self.assertFalse(actform.is_valid())
        self.assertEqual(
            actform.errors['nama'],
            ["This field is required."]
        )
        self.assertFalse(peepform.is_valid())
        self.assertEqual(
            peepform.errors['name'],
            ["This field is required."]
        )

    def test_story6_post_success_and_render_result(self):
        test = 'Anonymous'
        response_post = Client().post('/storysix/', {'nama': test})
        self.assertEqual(response_post.status_code, 200)

        response= Client().get('/storysix/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_peep_post_success_and_render_result(self):
        Activity.objects.create(nama='Painting')
        test = 'Syifaaa'
        response_post = Client().post('/storysix/peepform/1/', {'name': test})
        self.assertEqual(response_post.status_code, 200)

        response= Client().get('/storysix/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_peep_render_result(self):
        test = 'ThisisanActivity'
        testpeep = 'Ailee'
        a = Activity.objects.create(nama=test)
        People.objects.create(name=testpeep, activity=a)
        response= Client().get('/storysix/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)
        self.assertIn(testpeep, html_response)

    







 

    



    
