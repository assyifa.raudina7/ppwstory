from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm

class UserRegistrationForm(UserCreationForm):
    email = forms.EmailField()

    class Meta:
        model = User
        fields = ['username', 
                  'email', 
                  'password1', 
                  'password2'
        ]
        labels = {
            'username':"username", 
            'email':"username", 
            'password1':"password", 
            'password2':"confirm password"
        }


class LoginForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'type' : 'text',
        'placeholder': "Enter username",
        'required': True,
    }))
    password = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'type' : 'password',
        'placeholder': "Enter your password",
        'required': True,
    }))
    class Meta:
        labels = {
            'username' : "username",
            'password' : "password"
        }

