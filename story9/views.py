from django.shortcuts import render, redirect
from django.contrib import messages
from .forms import UserRegistrationForm, LoginForm
from django.contrib.auth import authenticate, login, logout



# Create your views here.
def register(request):
    if request.method == 'POST':
        regform = UserRegistrationForm(request.POST)
        if regform.is_valid():
            regform.save()
            username = regform.cleaned_data.get('username')
            messages.success(request, f'Account created for {username}!')
            return redirect('story9:login')
    else:
        regform = UserRegistrationForm()
    content = {'form': regform}
    return render(request, 'register.html', content)


def log_in(request):
    form = LoginForm()
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            usrname = form.cleaned_data['username']
            passwrd = form.cleaned_data['password']
            user = authenticate(request, username=usrname, password = passwrd)
            if user is not None:
                login(request, user)
                messages.success(request, f'Welcome, {usrname}!')
                return redirect('main:stories')
            else:
                messages.error(request,'username or password not correct')
                return redirect('login')
    else:
        response = {'form':form}
        return render(request,'login.html',response)

def log_out(request):
    logout(request)
    return render(request, 'logout.html')







