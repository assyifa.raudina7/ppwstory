from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import story7

# Create your tests here.
class TestStory7(TestCase):
    def test_url_story7_exists(self):
        #mencek url story 6 
        response = Client().get('/storyseven/')
        self.assertEqual(response.status_code, 200)
    
    def test_story7_def_storysix(self):
        found = resolve('/storyseven/')
        self.assertEqual(found.func, story7)
