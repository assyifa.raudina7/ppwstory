$(document).ready(function() {
    $(".accordion_header").click(function(e) {
        if ($(e.target).is("svg")) {
            return;
        } else {
            if ($(this).is(".accordion_header.active")) {
                $(this).removeClass("active");
            } else {
                $(this).addClass("active");
            }
        }
    });
    $(".up").click(function() {
        var self = $(this),
            item = self.parents("div.accordion_wrap"),
            swapWith = item.prev();
        item.after(swapWith.detach());
    });
    $(".down").click(function() {
        var self = $(this),
            item = self.parents("div.accordion_wrap"),
            swapWith = item.next();
        item.before(swapWith.detach());
    });
});