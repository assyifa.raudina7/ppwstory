from django.apps import AppConfig


class StoryoneConfig(AppConfig):
    name = 'storyone'
