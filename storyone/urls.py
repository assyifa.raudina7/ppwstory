from django.urls import path

from . import views

app_name = 'storyone'

urlpatterns = [
    path('', views.storyone, name='storyone'),
]
