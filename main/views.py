from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponseRedirect
from .forms import MatkulForm
from .models import Matkul, Post
from django.contrib.auth.decorators import login_required
response = {}

def home(request):
    return render(request, 'main/home.html')

def storyfive(request):
    return render(request, 'main/storyfive.html')

def contact(request):
    return render(request, 'main/contact.html')

def addmatkul(request):
    form  = MatkulForm(request.POST or None)
    if form.is_valid():
        form.save()
        form = MatkulForm()
    return render(request, 'main/addmatkul.html', {'form': form})

def daftarmatkul(request):
    matkuls = Matkul.objects.all()
    response['matkuls'] = matkuls
    html = 'main/daftarmatkul.html'
    print(response)
    return render(request, html, response)

def matkulcontent(request, id):
    obj = Matkul.objects.get(id= id)
    context = {
        "matkul": obj
    }
    return render(request, 'main/matkulcontent.html', context)

def deletematkul(request, id=None):
    obj = get_object_or_404(Matkul, id=id)
    obj.delete()
    return redirect('main:daftarmatkul')


@login_required
def stories(request):
    return render(request, 'main/stories.html')




