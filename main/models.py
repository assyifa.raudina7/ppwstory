from django.db import models
from django.utils import timezone
from datetime import datetime, date

# Create your models here.

class Matkul(models.Model):
    YEAR = (('gasal 2020/2021', 'Gasal 2020/2021'),
            ('genap 2019/2020', 'Genap 2019/2020'),
            ('gasal 2019/2020', 'Gasal 2019/2020'),
            ('genap 2018/2019', 'Genap 2018/2019'),
            ('gasal 2018/2019', 'Gasal 2018/2019')
            )
    nama = models.CharField(max_length=100, blank=False, null=False)
    dosen = models.CharField(max_length=100, blank=False, null=False)
    sks = models.IntegerField(blank=False, null=False)
    desc = models.TextField(blank=True, null=True)
    year = models.CharField(max_length=15, choices=YEAR, blank=True, null=True)
    ruang = models.CharField(max_length=10, blank=False, null=False)

    def _str_(self):
        return self.nama

class Post(models.Model):
    matkul = models.ForeignKey(Matkul, on_delete = models.CASCADE)
    content = models.CharField(max_length=200)
    published_date = models.DateTimeField(default=timezone.now)





