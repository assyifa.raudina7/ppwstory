from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse, resolve
from selenium import webdriver
from .views import home, addmatkul, daftarmatkul, contact, deletematkul, stories, storyfive
from .models import Matkul
from .forms import MatkulForm


@tag('functional')
class FunctionalTestCase(LiveServerTestCase):
    """Base class for functional test cases with selenium."""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()


class MainTestCase(TestCase):
    def test_root_url_status_200(self):
        response = self.client.get('')
        self.assertEqual(response.status_code, 200)
        # You can also use path names instead of explicit paths.
        response = self.client.get(reverse('main:home'))
        self.assertEqual(response.status_code, 200)

    def test_main_def_using_main_func(self):
        found = resolve('/')
        self.assertEqual(found.func, home)
        found2 = resolve('/contact/')
        self.assertEqual(found2.func, contact)
        found3 = resolve('/addmatkul/')
        self.assertEqual(found3.func, addmatkul)

    def test_url_stories(self):
        response = self.client.get('/stories/')
        self.assertEquals(response.status_code,302)

    def test_url_storyfive(self):
        response = self.client.get('/storyfive/')
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response,"main/storyfive.html")

    def test_url_contact(self):
        response = self.client.get('/contact/')
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response,"main/contact.html")

    def test_can_create_model_activity_and_people(self):
        #create new matkul          
        new_matkul = Matkul.objects.create(nama='Lisa', dosen='Manoban', sks=3, desc='lalalalllalalalalalala', year='Gasal 2020/2021', ruang='202A')
        #retrieve matkul
        counting_all_available_matkul = Matkul.objects.all().count()
        self.assertEqual(counting_all_available_matkul, 1)
        self.assertEqual(new_matkul._str_(), 'Lisa')

    def test_form_validation_for_blanks(self):
        form = MatkulForm(data={'nama':'', 'dosen':'', 'sks':3, 'desc':'','year':'', 'ruang':''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['nama'],
            ["This field is required."]
        )
        self.assertEqual(
            form.errors['dosen'],
            ["This field is required."]
        )
        self.assertEqual(
            form.errors['ruang'],
            ["This field is required."]
        )

    # def test_matkulform_post_success_and_render_the_result(self):
    #     test = 'Anonymous'
    #     response_post = Client().post('/addmatkul/', {'nama':test, 'dosen':'Manoban', 'sks':3, 'desc':'lalalalallal','year':'Gasal 2020/2021', 'ruang':'202A'})
    #     self.assertEqual(response_post.status_code, 200)
    #     self.assertEqual(Matkul.objects.all().count(), 1)

    #     response = self.client.get('/daftarmatkul/')
    #     html_response = response.content.decode('utf8')
    #     self.assertIn(test, html_response)

    #     response_detail = self.client.get('/matkulcontent/1/')
    #     html_response = response.content.decode('utf8')
    #     self.assertIn('Manoban', html_response)
    #     self.assertIn('lalalalallal', html_response)



class MainFunctionalTestCase(FunctionalTestCase):
    def test_root_url_exists(self):
        self.selenium.get(f'{self.live_server_url}/')
        html = self.selenium.find_element_by_tag_name('html')
        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())
