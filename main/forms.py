from django import forms

from .models import Matkul

class MatkulForm(forms.ModelForm):
    class Meta:
        model = Matkul
        fields = [
            'nama',
            'dosen',
            'sks',
            'desc',
            'year',
            'ruang'
        ]
        widgets = {
            'nama': forms.TextInput(attrs={'class': 'form-control'}),
            'dosen': forms.TextInput(attrs={'class': 'form-control'}),
            'sks': forms.NumberInput(attrs={'class': 'form-control'}),
            'desc': forms.Textarea(attrs={'class': 'form-control'}),
            'year': forms.Select(attrs={'class': 'form-control'}),
            'ruang': forms.TextInput(attrs={'class': 'form-control'}),
        }
