from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('contact/', views.contact, name='contact'),
    path('addmatkul/', views.addmatkul, name='addmatkul'),
    path('daftarmatkul/', views.daftarmatkul, name='daftarmatkul'),
    path('matkulcontent/<int:id>/', views.matkulcontent, name='matkulcontent'),
    path('daftarmatkul/delete/<int:id>/', views.deletematkul, name='deletematkul'),
    path('stories/', views.stories, name='stories'),
    path('storyfive/', views.storyfive, name='storyfive'),
]
