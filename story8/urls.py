from django.urls import path

from . import views

app_name = 'story8'

urlpatterns = [
    path('', views.story8, name='story8'),
    path('data/', views.data, name='data'),
]