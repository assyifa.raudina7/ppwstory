from django.shortcuts import render
from django.http import JsonResponse
import requests, json

# Create your views here.
def story8(request):
    response = {}
    return render(request, 'storyeight.html', response)
    
def data(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
    ret = requests.get(url)
    data = json.loads(ret.content)
    return JsonResponse(data, safe=False)