from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import story8, data

class TestStory8(TestCase):
	
	def test_home_url_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	def test_url_book_api_ajax(self):
		response = Client().get("/storyeight/data/?q=test")
		self.assertEqual(response.status_code, 200)        

	def test_home_using_right_template(self):
		response = Client().get('/storyeight/')
		self.assertTemplateUsed(response,'storyeight.html')
        

	def test_calling_right_views_function(self):
		found = resolve('/storyeight/')
		self.assertEqual(found.func, story8)

    # def test_calling_right_data_view_function(self):
	# 	found = resolve('/storyeight/data/')
	# 	self.assertEqual(found.func, data)

