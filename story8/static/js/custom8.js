$(document).ready(function() {
    $("#keyword").keyup(function() {
        var carian = $("#keyword").val();
        $.ajax({
            url: 'data?q=' + carian,
            success: function(data) {
                var content = '';
                var array_items = data.items;
                $("#daftar_isi").empty();
                for (i = 0; i < array_items.length; i++) {
                    var judul = array_items[i].volumeInfo.title;
                    var gambar = array_items[i].volumeInfo.imageLinks;
                    var author = array_items[i].volumeInfo.authors;
                    var linq = array_items[i].volumeInfo.infoLink;
                    var alt_gambar = '';

                    if (typeof judul === 'undefined') {
                        judul = 'Title not found'
                    }


                    if (typeof gambar === 'undefined') {
                        gambar = '#'
                        alt_gambar = 'Image not found'
                    } else {
                        gambar = gambar.thumbnail;
                    }


                    if (typeof author === 'undefined') {
                        author = 'Author not found'
                    } else {
                        author = author.join(' - ')
                    }

                    content += '<tr>' +
                        '<th scope="row" class="tabelrow">' + (i + 1) + '</th>' +
                        '<td class="tabelrow"><a style="font-size:0;" href="' + linq + '">' + '<img class ="buku" src="' + gambar + '" alt="' + alt_gambar + '"></a></td>' +
                        '<td class="tabelrow">' + judul + '</td>' +
                        '<td class="tabelrow">' + author + '</td>' +
                        '</tr>'
                }
                $("#daftar_isi").append(
                    '<table class="table tabelbook"><thead>' +
                    '<tr>' +
                    '<th scope="col" class="tabelheader" style="border-top-left-radius: 0.5rem;">No.</th>' +
                    '<th scope="col" class="tabelheader">Image</th>' +
                    '<th scope="col" class="tabelheader">Title</th>' +
                    '<th scope="col" class="tabelheader" style=" border-top-right-radius: 0.5rem;">Author</th>' +
                    '</tr></thead>' + content + "</table>"
                );
            }
        });
    });
});